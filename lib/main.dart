import 'package:flutter/material.dart';
import 'users.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Future<List<Users>> fetchUsers(BuildContext context) async {
    final jsonstring =
        await DefaultAssetBundle.of(context).loadString('assets/users.json');
    return usersFromJson(jsonstring);
// calling usersFromJson from users.dart to read the data
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('json demo'),
      ),
      body: Container(
        child: FutureBuilder(
            future: fetchUsers(context),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                  itemCount: snapshot.data.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    Users item = snapshot.data[index];
                    return Card(
                      color: Colors.amber[100],
                      margin: EdgeInsets.all(15),
                      child: Padding(
                        padding: EdgeInsets.all(16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              item.name,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 28,
                                  color: Colors.green),
                            ),
                            Text(
                              item.username,
                            ),
                            Text(
                              item.email,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "Address",
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              item.address.street,
                            ),
                            Text(
                              item.address.suite,
                            ),
                            Text(
                              item.address.city,
                            ),
                            Text(
                              item.address.zipcode,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "Geo",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              item.address.geo.lat,
                            ),
                            Text(
                              item.address.geo.lng,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "Phone number",
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              item.phone,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "Website",
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              item.website,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "Company",
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              item.company.name,
                            ),
                            Text(
                              item.company.catchPhrase,
                            ),
                            Text(
                              item.company.bs,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                );
              }
              return CircularProgressIndicator();
            }),
      ),
    );
  }
}
